# My First Programming Language

The goal of this project is to help you solve your Formal Languages and Compilers homeworks.

For each of the four homeworks there will a be a commit with some examples that should help you get started.

Installation of Flex and Bison is not required on Windows, as it is included in the repository. I recommend to use a C++ compiler which supports the C++17 standard, as I will be using it in the examples. 

For more details, see the comments in the code.
