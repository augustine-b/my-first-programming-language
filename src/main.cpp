#include "driver.h"

/**
 * Run the following before compilation:
 *  Linux:
 *      flex -o generated-src/lexer.cpp src/lexer.lpp
 *      bison -o generated-src/parser.cpp src/parser.ypp
 *
 *  Windows:
 *      .\win_flex.exe -o generated-src\lexer.cpp src\lexer.lpp
 *      .\win_bison.exe -o generated-src\parser.cpp src\parser.ypp
 */

/**
 * First and second homework: FLEX and BISON
 *
 *      1) plan your programming language in detail!
 *      2) based on the plan decide on the terminal symbols of the language
 *          (e.g. integers, decimals, booleans, identifiers, the different operators, etc.)
 *      3) define these tokens in the parser.ypp file
 *      4) extend lexer.lpp such that for each matched token it prints it like in the examples
 *          and then returns the right type of token to the parser
 *      5) define the grammar of your language in the parser.ypp file
 *      6) fix all the shift-shift and shift-reduce conflicts you encounter
 *          (use -Wcounterexamples when calling bison)
 *
 * Third homework: type checking
 *
 *      1) subclass Node with all possible nodes in your program (declaration node, if node,
 *          assignment node and so on)
 *      2) implement the type checking method on all of the subclasses
 *      3) make use of the symbol table to keep track of the declared variables and their types
 *      4) disallow redeclaration of variables, but allow them to be shadowed by new variables
 *          inside blocks
 *      5) make sure variables cannot be used outside the block they were declared in
 *      6) create the right nodes inside the parser actions (if you're struggling with unique
 *          pointers you may use regular raw pointers instead)
 *
 *
 * A very useful resource for parsing expressions (the homework files are based on this):
 *      https://www.gnu.org/software/bison/manual/html_node/Calc_002b_002b-Parser.html
 */

int main() {
    Driver driver {};

    std::unique_ptr<Node> programTree = driver.parse_file("../inputs/expression_input.txt");
    if (!programTree) {
        std::cout << "Parsing failed." << std::endl;
        return 1;
    }

    std::cout << "Parsing succeeded!" << std::endl;

    try {
        programTree->check_type();
        std::cout << "Type checking succeeded!" << std::endl;
    } catch (TypeError& e) {
        std::cout << "Type checking failed, reason: " << e.message << std::endl;
        return 1;
    }

    return 0;
}
