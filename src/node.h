#ifndef NODE_H
#define NODE_H

#include <memory>
#include <utility>
#include <vector>
#include <map>
#include <stack>
#include <string>

enum class ValueType {
    INTEGER,
    BOOLEAN,
};

enum class BinaryOperator {
    AND,
    LESS_THAN,
    PLUS,
    TIMES,
};

struct TypeError : std::exception {
    const std::string message;
    explicit TypeError(std::string message) : message { std::move(message) } {};
};

struct Node {
    virtual ~Node() = default;

    ValueType check_type() {
        std::stack<std::map<std::string, ValueType>> symbolTable {};
        symbolTable.push({}); // initialize the symbol table with an empty map
        return check_type(symbolTable);
    }
    virtual ValueType check_type(std::stack<std::map<std::string, ValueType>>& symbolTable) = 0;
};

struct IntegerNode : Node {
    int value;

    explicit IntegerNode(int value) : value { value } {}

    ValueType check_type(std::stack<std::map<std::string, ValueType>> &symbolTable) override {
        return ValueType::INTEGER;
    }
};

struct BinaryOperatorNode : Node {
    const BinaryOperator op;
    const std::unique_ptr<Node> left;
    const std::unique_ptr<Node> right;

    BinaryOperatorNode(BinaryOperator op, std::unique_ptr<Node> left, std::unique_ptr<Node> right) :
            op { op }, left { std::move(left) }, right { std::move(right) } {};

    ValueType check_type(std::stack<std::map<std::string, ValueType>>& symbolTable) override {
        ValueType leftType = left->check_type(symbolTable);
        ValueType rightType = right->check_type(symbolTable);

        switch (op) {
            // arithmetic operators
            case BinaryOperator::PLUS:
            case BinaryOperator::TIMES:
                if (leftType == ValueType::INTEGER && rightType == ValueType::INTEGER) {
                    return ValueType::INTEGER;
                }
                break;

            // comparison operators
            case BinaryOperator::LESS_THAN:
                if (leftType == ValueType::INTEGER && rightType == ValueType::INTEGER) {
                    return ValueType::BOOLEAN;
                }
                break;

            // logical operators
            case BinaryOperator::AND:
                if (leftType == ValueType::BOOLEAN && rightType == ValueType::BOOLEAN) {
                    return ValueType::BOOLEAN;
                }
                break;
        }

        throw TypeError("Invalid types <> and <> for binary operator <>");
    }
};

#endif
