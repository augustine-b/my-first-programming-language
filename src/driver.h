#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include "../generated-src/parser.hpp"

# define YY_DECL \
  yy::parser::symbol_type yylex (Driver& drv)

YY_DECL;

class Driver
{
public:
    std::string file;

    yy::location location;

    Driver() : file {}, location {} {};

    std::unique_ptr<Node> parse_file(const std::string &file);

    void begin_file(const std::string& file);
};

#endif
