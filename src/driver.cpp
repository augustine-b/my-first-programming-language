#include "driver.h"

std::unique_ptr<Node> Driver::parse_file(const std::string &f) {
    file = f;

    location.initialize (&file);
    begin_file(file);

    std::unique_ptr<Node> output;
    yy::parser parser {*this, output};

    if (parser.parse() == 0) {
        return output;
    } else {
        return {};
    }
}
